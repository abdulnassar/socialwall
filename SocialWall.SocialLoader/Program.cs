﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SocialWall.SocialLoader
{
    class Program
    {
        static  void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Loading Instagram data started");
                 new System.Net.WebClient().DownloadString(General.GetSettingsValue("Instagram_sync_url"));
                Console.WriteLine("Loading Instagram data completed");

            }
            catch (Exception ex)
            {
               
            }
        }
    }
}
