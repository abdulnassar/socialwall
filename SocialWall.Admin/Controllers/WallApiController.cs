﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SocialWall.Admin.Models;
using System.Data;

namespace SocialWall.Admin.Controllers
{
    
    public class WallApiController : ApiController
    {
        // GET api/<controller>
        public List<Wall> Get()
        {
            List<Wall> wallList = new List<Wall>();

            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    SqlHelper sqlHelper = new SqlHelper();

                    DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetWallList");

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            wallList.Add(new Wall
                            {
                                Id = Convert.ToInt32(dr["Id"].ToString()),
                                Wall_name = dr["Wall_name"].ToString(),
                                Wall_title = dr["Wall_title"].ToString(),
                                Wall_details = dr["Wall_details"].ToString(),
                                Wall_refresh_seconds = Convert.ToInt32(dr["Wall_refresh_seconds"].ToString()),
                                IsEnabled = bool.Parse(dr["IsEnabled"].ToString())
                            });
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                General.LogException(ex);
            }
            return wallList;

        }

        //// GET api/<controller>/5
        public Wall Get(int id)
        {
            Wall wall = new Wall();

            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    Wall dataModel = new Wall();

                    wall = dataModel.Get(id);
                }
            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return wall;
        }

        // POST api/<controller>
        public ReturnData Post([FromBody]WallViewModel model)
        {
            ReturnData returnData = new ReturnData();
            if (ModelState.IsValid)
            {
                try
                {
                    if (System.Web.HttpContext.Current.Session["UserId"] != null)
                    {
                        Wall dataModel = new Wall();
                        dataModel.Wall_name = model.Wall_name;
                        dataModel.Wall_title = model.Wall_title;
                        dataModel.Wall_details = model.Wall_details;
                        dataModel.Wall_refresh_seconds = model.Wall_refresh_seconds;
                        dataModel.Created_by = System.Web.HttpContext.Current.Session["UserId"].ToString();

                        if (dataModel.SaveWall())
                        {
                            returnData.operationStatus = "SUCCESS";
                        }

                      
                    }
                }
                catch(Exception ex)
                {
                    returnData.operationStatus = "EXCEPTION";
                    General.LogException(ex);

                }
              
            }
            else
            {
                returnData.operationStatus = "INVALID";
            }

            return returnData;
        }

        // PUT api/<controller>/5
       
        public ReturnData Put(int id, [FromBody]WallViewModel model)
        {
            ReturnData returnData = new ReturnData();

            SqlHelper sqlHelper = new SqlHelper();
            if (ModelState.IsValid)
            {
              
                try
                {
                    if (System.Web.HttpContext.Current.Session["UserId"] != null)
                    {
                        Wall dataModel = new Wall();
                        dataModel.Wall_name = model.Wall_name;
                        dataModel.Wall_title = model.Wall_title;
                        dataModel.Wall_details = model.Wall_details;
                        dataModel.Wall_refresh_seconds = model.Wall_refresh_seconds;
                        dataModel.IsEnabled = model.IsEnabled;

                        if (dataModel.Update(id, model.nsvar))
                        {
                            returnData.operationStatus = "SUCCESS";
                        }


                      
                    }
                   
                }
                catch (Exception ex)
                {
                    returnData.operationStatus = "EXCEPTION";
                    General.LogException(ex);

                }

            }
            else
            {
                returnData.operationStatus = "INVALID";
            }

            if (model.nsvar=="STATUSUPDATE")
            {
                try
                {
                    if (System.Web.HttpContext.Current.Session["UserId"] != null)
                    {
                        Wall dataModel = new Wall();
                        dataModel.IsEnabled = model.IsEnabled;

                        if (dataModel.Update(id, model.nsvar))
                        {
                            returnData.operationStatus = "SUCCESS";
                        }
                    }
                    else
                    {
                        returnData.operationStatus = "SESSIONOUT";
                    }
                }
                catch (Exception ex)
                {
                    returnData.operationStatus = "EXCEPTION";
                    General.LogException(ex);

                }

            }

            return returnData;
        }


        // DELETE api/<controller>/5
        public ReturnData Delete(int id)
        {
            ReturnData returnData = new ReturnData();
            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    SqlHelper sqlHelper = new SqlHelper();
                    sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);


                    sqlHelper.GetExecuteNonQueryByCommand("SP_DeleteWall");

                    returnData.operationStatus = "SUCCESS";
                }
                else
                {
                    returnData.operationStatus = "SESSIONOUT";
                }
            }
            catch (Exception ex)
            {
                returnData.operationStatus = "EXCEPTION";
                General.LogException(ex);

            }

            return returnData;
        }
    }
}