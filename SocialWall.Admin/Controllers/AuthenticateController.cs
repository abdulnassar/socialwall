﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;

namespace SocialWall.Admin.Controllers
{
    [AuthorizeUser(AccessLevel = "Admin")]
    public class AuthenticateController : Controller
    {
        // GET: Authenticate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Instagram()
        {
            Response.Redirect(General.GetSettingsValue("Instagram_auth_base") +"?client_id="
                +General.GetSettingsValue("Instagram_client_id") 
                +"&redirect_uri="+ General.GetSettingsValue("Instagram_redirect_uri") + "&response_type=code"
                + "&scope=public_content");
            return View();
        }
        
        public async System.Threading.Tasks.Task<ActionResult> Instagramresp(string code)
        {
            try
            {
                HttpClient client = new HttpClient();
                var values = new Dictionary<string, string>
                {
                   { "client_id", General.GetSettingsValue("Instagram_client_id")  },
                   { "client_secret", General.GetSettingsValue("Instagram_client_secret")  },
                   { "grant_type", "authorization_code" },
                   { "redirect_uri", General.GetSettingsValue("Instagram_redirect_uri") },
                   { "code", code }
                };

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync(General.GetSettingsValue("Instagram_accesstoken_base"), content);

                var responseString = await response.Content.ReadAsStringAsync();

                InstaAccessCode instaAccessCode = Newtonsoft.Json.JsonConvert.DeserializeObject<InstaAccessCode>(responseString);

                General.SetSettingsValue("Instagram_access_token", instaAccessCode.access_token);
                General.SetSettingsValue("Instagram_access_token_status", "OK");

                Response.Write("Successfully authenticated. Please close this window.");
            }
            catch(Exception ex)
            {
                General.LogException(ex);
            }

            return View();
        }
        
    }

    public class InstaAccessCode
    {
        public string access_token { get; set; }
        public InstaTokenUser user { get; set; }
    }

    public class InstaTokenUser
    {
        public string id { get; set; }
        public string username { get; set; }
        public string full_name { get; set; }
        public string profile_picture { get; set; }
    }
}