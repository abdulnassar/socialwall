﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialWall.Admin.Models;

namespace SocialWall.Admin.Controllers
{
    [AuthorizeUser(AccessLevel = "Admin")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            Alerts model = new Alerts();

            model.alertList = new List<Alert>();

            try
            {
                if (General.GetSettingsValue("Instagram_access_token_status") != "OK")
                {
                    model.alertList.Add(new Alert
                    {
                        alertType = "Warning",
                        message = "Your Instagram access token has been expired",
                        actionLink = General.GetSettingsValue("Admin_baseurl") +"authenticate/instagram",
                        actionMessage = "Please set new token"
                    });
                }

                model.alertList.Add(new Alert
                {
                    alertType = "Authenticate",
                    message = "Set your Instagram access token",
                    actionLink = General.GetSettingsValue("Admin_baseurl") + "authenticate/instagram",
                    actionMessage = "Login to Instagram"
                });
            }
            catch
            {

            }

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}