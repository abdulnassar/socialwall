﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SocialWall.Admin.Models;
using System.Data;


namespace SocialWall.Admin.Controllers
{
   
    public class SourceApiController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public List<Source> Get(int id)
        {
            List<Source> sourceList = new List<Source>();

            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    Source dataModel = new Source();
                    sourceList = dataModel.GetSourceList(id);
                  
                }
                else
                {
                   // returnData.operationStatus = "SESSIONOUT";
                }
            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return sourceList;
        }

        // POST api/<controller>
        public async System.Threading.Tasks.Task<ReturnData> Post([FromBody]SourceViewModel model)
        {
            ReturnData returnData = new ReturnData();
            if (ModelState.IsValid)
            {
                try
                {
                    if (System.Web.HttpContext.Current.Session["UserId"] != null)
                    {
                     

                        Source dataModel = new Source();
                        dataModel.Source_name = model.Source_name;
                        dataModel.Source_type = model.Source_type;
                        dataModel.Source_socialmedia_name = model.Source_socialmedia_name;
                        dataModel.Wall_id = model.Wall_id;
                        dataModel.Created_by = System.Web.HttpContext.Current.Session["UserId"].ToString();
                        dataModel.Source_name = model.Source_name;

                        if (model.Source_socialmedia_name == "Instagram")
                        {
                            if (model.Source_type == "Tag")                               
                            {
                                dataModel.Source_social_id = model.Source_name;
                               

                            }
                            else if (model.Source_type == "User")
                            {
                                HttpClient client = new HttpClient();
                                client = new HttpClient();

                                var response = await
                                    client.GetAsync(
                                        "https://api.instagram.com/v1/users/search?q=" + model.Source_name
                                        + "&access_token=" + General.GetSettingsValue("Instagram_access_token")
                                        );

                                var responseString = await response.Content.ReadAsStringAsync();
                                // returnData.message = responseString;
                                InstaUsers instaUsers = Newtonsoft.Json.JsonConvert.DeserializeObject<InstaUsers>(responseString);
                                foreach (Datum datum in instaUsers.data)
                                {
                                    

                                    dataModel.Source_social_id = datum.id;
                                }


                            }
                        }

                        if (dataModel.SaveSource())
                        {
                            returnData.operationStatus = "SUCCESS";

                        }

                      
                    }
                    else
                    {
                        returnData.operationStatus = "SESSIONOUT";
                    }
                }
                catch (Exception ex)
                {
                    returnData.operationStatus = "EXCEPTION";
                    General.LogException(ex);

                }

            }
            else
            {
                returnData.operationStatus = "INVALID";
            }

            return returnData;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public ReturnData Delete(int id)
        {
            ReturnData returnData = new ReturnData();
           
            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {


                    Source dataModel = new Source();
                    if (dataModel.DeleteSource(id))
                    {
                        returnData.operationStatus = "SUCCESS";
                    }

                    
                }
                else
                {
                    returnData.operationStatus = "SESSIONOUT";
                }
            }
            catch (Exception ex)
            {
                returnData.operationStatus = "EXCEPTION";
                General.LogException(ex);

            }

            return returnData;
        }
    }


    public class InstaUsers
    {
        public Datum[] data { get; set; }
        public Meta meta { get; set; }
    }

    public class Meta
    {
        public int code { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public string username { get; set; }
        public string profile_picture { get; set; }
        public string full_name { get; set; }
        public string bio { get; set; }
        public string website { get; set; }
        public bool is_business { get; set; }
    }

}