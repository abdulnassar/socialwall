﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SocialWall.Admin.Models;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Data;
using System.DirectoryServices;

namespace SocialWall.Admin.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

       
        public ActionResult LogOff()
        {
        
            Session.Abandon();       
            return RedirectToAction("Login", "Account");
        }

        public ActionResult ForgotPassword()
        {

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                { 
                    string token = System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 20);

                    //save the token in db

                    AccountModel dataModel = new AccountModel();
                    dataModel.Email = model.Email;
                    dataModel.Password_token = token;

                    if (dataModel.SaveForgotPassword())
                    {
                        string body = "VOX Social Wall <br/> Reset password link: "
                       + General.GetSettingsValue("Admin_baseurl") + "Account/ResetPassword?email=" + model.Email + "&token=" + token;

                        General.SendEmail("VOX Social Wall- Reset Password", body, "voxmails@maf.co.ae", "VOX CINEMAS", model.Email);

                        return RedirectToAction("EmailSent", "Account");
                    }
                }
                catch (Exception ex)
                {
                    General.LogException(ex);                   
                   
                }
                
            }
            return View();
        }
        public ActionResult EmailSent()
        {


            return View();
        }
        public ActionResult ResetPassword(string email,string token)
        {
            try
            {

                AccountModel dataModel = new AccountModel();
                dataModel.Email = email;
                dataModel.Password_token = token;

                if (dataModel.ValidatePasswordToken())
                {
                    ResetPasswordViewModel model = new ResetPasswordViewModel();
                    model.Email = email;
                    model.Token = token;                    
                    return View(model);
                }            

            }
            catch (Exception ex)
            {
                General.LogException(ex);

            }

            return RedirectToAction("InvalidToken", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    AccountModel dataModel = new AccountModel();
                    dataModel.Email = model.Email;
                    dataModel.Password_token = model.Token;
                    dataModel.Password = model.Password;

                    if (dataModel.ResetPAssword())
                    {
                        return RedirectToAction("ResetSuccess", "Account");
                    }
                    else
                    {
                        ViewBag.error = "Error";
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.error = "Error";
                    General.LogException(ex);

                }
            }
            else
            {
                ViewBag.error = "Error";

            }

            return View(model);
        }

        public ActionResult ResetSuccess()
        {

            return View();
        }

        public ActionResult InvalidToken()
        {
            
            return View();
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
          

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            try
            {
                AccountModel dataModel = new AccountModel();
                dataModel.Email = model.Email;               
                dataModel.Password = model.Password;

                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@Email", SqlDbType.VarChar, model.Email);
                sqlHelper.AddSetParameterToSQLCommand("@Password", SqlDbType.VarChar, model.Password);
                DataSet ds = sqlHelper.GetDatasetByCommand("[SP_GetUser]");

                if (dataModel.Login())
                {
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
                }

               

                //    using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, ConfigurationManager.AppSettings["domain"].ToString()))
                //    {
                //        // validate the credentials
                //        bool isValid = pc.ValidateCredentials(model.Username, model.Password);

                //        if (isValid)
                //        {

                //            SqlHelper sqlHelper = new SqlHelper();
                //            sqlHelper.AddSetParameterToSQLCommand("@Username", SqlDbType.VarChar, model.Username);
                //            DataSet ds = sqlHelper.GetDatasetByCommand("[SP_GetUser]");




                //            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                //            {
                //                if (ds.Tables[0].Rows.Count > 0)
                //                {
                //                    ResultPropertyCollection resultPropertyCollection = getDataFromAD(model.Username);

                //                    Session["nameOfUser"] = resultPropertyCollection["name"][0].ToString();

                //                    Session["UserId"] = ds.Tables[0].Rows[0]["Id"].ToString();


                //                    string UserRoles = "Admin,Operations";


                //                    Session["privilegeLevels"] = UserRoles;




                //                    return RedirectToLocal(returnUrl);
                //                }
                //                else
                //                {
                //                    ModelState.AddModelError("", "Invalid login attempt.");
                //                    return View(model);
                //                }
                //            }
                //            else
                //            {
                //                ModelState.AddModelError("", "Invalid login attempt.");
                //                return View(model);
                //            }

                //        }
                //        else
                //        {
                //            ModelState.AddModelError("", "Invalid login attempt.");
                //            return View(model);
                //        }

                //    }
            }
            catch (Exception ex)
            {
                General.LogException(ex);
                ModelState.AddModelError("", "Invalid login attempt.");
                return View(model);
            }


        }

        private ResultPropertyCollection getDataFromAD(string uid)
        {
            DirectorySearcher dirSearcher = new DirectorySearcher();
            DirectoryEntry entry = new DirectoryEntry(dirSearcher.SearchRoot.Path);
            dirSearcher.Filter = "(&(objectClass=user)(objectcategory=person)(samaccountname=" + uid + "*))";

            SearchResult searchResult = dirSearcher.FindOne();

            // string propName = popField ;
            // ResultPropertyValueCollection valColl = searchResult.Properties[propName];
            try
            {
                //return valColl[0].ToString();

                return searchResult.Properties;
            }
            catch
            {
                return null;
            }

        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

    }
}