﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SocialWall.Admin.Models;
using System.Data;

namespace SocialWall.Admin.Controllers
{
   
    public class FeedApiController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public List<Feed> Get(int id)
        {
            List<Feed> feedList = new List<Feed>();

            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    SqlHelper sqlHelper = new SqlHelper();

                    sqlHelper.AddSetParameterToSQLCommand("@Wall_id", SqlDbType.Int, id);
                    DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetFeedList");

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            feedList.Add(new Feed
                            {
                                // Id = Convert.ToInt32(dr["Id"].ToString()),
                                IsEnabled = bool.Parse(dr["IsEnabled"].ToString()),
                                Social_id = dr["Social_id"].ToString(),
                                Social_image_thumbnail = dr["Social_image_thumbnail"].ToString(),
                                Social_image_low_resolution = dr["Social_image_low_resolution"].ToString(),
                                Social_image_standard_resolution = dr["Social_image_standard_resolution"].ToString(),
                                Social_caption_text = dr["Social_caption_text"].ToString(),
                                Social_user_id = dr["Social_user_id"].ToString(),
                                Social_user_full_name = dr["Social_user_full_name"].ToString(),
                                Social_user_profile_picture = dr["Social_user_profile_picture"].ToString(),
                                Social_user_username = dr["Social_user_username"].ToString(),                               
                                Feed_socialmedia_name = dr["Feed_socialmedia_name"].ToString(),
                                Social_likes_count = dr["Social_likes_count"].ToString(),
                                Social_tags_count = dr["Social_tags_count"].ToString(),
                                Social_comments_count = dr["Social_comments_count"].ToString()

                                  

                            });
                        }
                    }
                }
                else
                {
                  //  returnData.operationStatus = "SESSIONOUT";
                }
            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return feedList;
        }

        
        public ReturnData Put(int id, [FromBody]FeedViewModel model)
        {
            ReturnData returnData = new ReturnData();

            SqlHelper sqlHelper = new SqlHelper();
            if (ModelState.IsValid)
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    try
                    {
                        Feed dataModel = new Feed();
                        dataModel.Id = id;
                        dataModel.Feed_socialmedia_name = model.Feed_socialmedia_name;
                        dataModel.Feed_source_social_id = model.Feed_source_social_id;
                        dataModel.IsEnabled = model.IsEnabled;

                        dataModel.FilterFeed();


                        returnData.operationStatus = "SUCCESS";


                    }
                    catch (Exception ex)
                    {
                        returnData.operationStatus = "EXCEPTION";
                        General.LogException(ex);

                    }
                }
                else
                {
                    returnData.operationStatus = "SESSIONOUT";
                }
            }
            else
            {
                returnData.operationStatus = "INVALID";
            }

         

            return returnData;
        }

        
    }



}