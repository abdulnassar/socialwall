﻿using SocialWall.Admin.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SocialWall.Admin.Controllers
{
    [AuthorizeUser(AccessLevel = "Admin")]
    public class WallController : Controller
    {
        // GET: Wall
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Update(string id)
        {
            Wall wall = new Wall();
            wall.Id = Convert.ToInt32(id);

            return View(wall);
        }
    }
}