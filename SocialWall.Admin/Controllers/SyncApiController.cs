﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SocialWall.Admin.Controllers
{
   
    public class SyncApiController : ApiController
    {
        // GET api/<controller>
        public async System.Threading.Tasks.Task<string> Get()
        {
            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    HttpClient client = new HttpClient();
                    client = new HttpClient();

                    var response = await
                        client.GetAsync(General.GetSettingsValue("Instagram_sync_url"));
                }
                else
                {
                   // returnData.operationStatus = "SESSIONOUT";
                }
            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }


            return "SUCCESS";
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}