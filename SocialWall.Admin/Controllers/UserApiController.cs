﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SocialWall.Admin.Models;
using System.Data;

namespace SocialWall.Admin.Controllers
{
  
    public class UserApiController : ApiController
    {
      

        // GET api/<controller>/5
        public List<AppUser> Get()
        {
            List<AppUser> userList = new List<AppUser>();

            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    AppUser dataModel = new AppUser();
                    userList = dataModel.GetUserList();
                }
                else
                {
                    //returnData.operationStatus = "SESSIONOUT";
                }

            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return userList;
        }

        // POST api/<controller>
        public async System.Threading.Tasks.Task<ReturnData> Post([FromBody]AppUser model)
        {
            ReturnData returnData = new ReturnData();
            if (ModelState.IsValid)
            {
                try
                {
                    if (System.Web.HttpContext.Current.Session["UserId"] != null)
                    {

                        AppUser dataModel = new AppUser();
                        dataModel.Email = model.Email;
                        dataModel.Password = model.Password;
                        dataModel.Created_by = System.Web.HttpContext.Current.Session["UserId"].ToString();

                        if (dataModel.SaveUser())
                        {
                            returnData.operationStatus = "SUCCESS";
                        }

                        
                    }
                    else
                    {
                        returnData.operationStatus = "SESSIONOUT";
                    }
                }
                catch (Exception ex)
                {
                    returnData.operationStatus = "EXCEPTION";
                    General.LogException(ex);

                }

            }
            else
            {
                returnData.operationStatus = "INVALID";
            }

            return returnData;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public ReturnData Delete(int id)
        {
            ReturnData returnData = new ReturnData();
            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {

                    AppUser dataModel = new AppUser();
                  
                   

                    if (dataModel.DeleteUser(id))
                    {
                        returnData.operationStatus = "SUCCESS";
                    }

                }
                else
                {
                    returnData.operationStatus = "SESSIONOUT";
                }
            }
            catch (Exception ex)
            {
                returnData.operationStatus = "EXCEPTION";
                General.LogException(ex);

            }

            return returnData;
        }
    }



}