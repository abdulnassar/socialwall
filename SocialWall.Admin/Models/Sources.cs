﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace SocialWall.Admin.Models
{
    public class Sources
    {
        

        
    }

    public class Source
    {
        public int Id { get; set; }
        public int Wall_id { get; set; }
        public string Source_name { get; set; }
        public string Source_type { get; set; }
        public string Source_social_id { get; set; }        
        public string Source_socialmedia_name { get; set; }
        public string Created_by { get; set; }
        

        public List<Source> GetSourceList(int id)
        {
            List<Source> sourceList = new List<Source>();
            try
            {
               
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Wall_id", SqlDbType.Int, id);
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetSourcesList");

                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        sourceList.Add(new Source
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            Source_name = dr["Source_name"].ToString(),
                            Source_type = dr["Source_type"].ToString(),
                            Source_socialmedia_name = dr["Source_socialmedia_name"].ToString()

                        });
                    }
                }
            }
            catch (Exception ex)
            {

                General.LogException(ex);

            }
            return sourceList;
        }

        public bool SaveSource()
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Source_name", SqlDbType.VarChar, Source_name);
                sqlHelper.AddSetParameterToSQLCommand("@Source_type", SqlDbType.VarChar, Source_type);
                sqlHelper.AddSetParameterToSQLCommand("@Source_socialmedia_name", SqlDbType.VarChar, Source_socialmedia_name);
                sqlHelper.AddSetParameterToSQLCommand("@Wall_id", SqlDbType.Int, Wall_id);
                sqlHelper.AddSetParameterToSQLCommand("@Created_by", SqlDbType.Int, Created_by);
                sqlHelper.AddSetParameterToSQLCommand("@Source_social_id", SqlDbType.VarChar, Source_social_id);

                sqlHelper.GetExecuteNonQueryByCommand("SP_SaveSource");

                return true;
            }
            catch (Exception ex)
            {

                General.LogException(ex);

            }

            return false;
        }

        public bool DeleteSource(int id)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);


                sqlHelper.GetExecuteNonQueryByCommand("SP_DeleteSource");

                return true;
            }
            catch (Exception ex)
            {

                General.LogException(ex);

            }

            return false;
        }

    }

    public class SourceViewModel
    {
        [Required(ErrorMessage = "Wall id required")]
        public int Wall_id { get; set; }

        [Required(ErrorMessage = "Source name required")]
        [DataType(DataType.Text, ErrorMessage = "Source name is not valid")]
        public string Source_name { get; set; }

        [Required(ErrorMessage = "Source type required")]
        [DataType(DataType.Text, ErrorMessage = "Source type is not valid")]
        public string Source_type { get; set; }

        [Required(ErrorMessage = "Social media required")]
        [DataType(DataType.Text, ErrorMessage = "Social media is not valid")]
        public string Source_socialmedia_name { get; set; }


        public string nsvar { get; set; }
    }

}