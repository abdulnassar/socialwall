﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialWall.Admin.Models
{
    public class Alerts
    {
        public List<Alert> alertList { get; set; }
       
    }

   

    public class Alert
    {
        public string alertType { get; set; }
        public string message { get; set; }
        public string actionLink { get; set; }
        public string actionMessage { get; set; }
        
    }
}