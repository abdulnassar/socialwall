﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SocialWall.Admin.Models
{
    public class AppUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Created_by { get; set; }
        
        public List<AppUser> GetUserList()
        {
            List<AppUser> userList = new List<AppUser>();
            try
            {

                SqlHelper sqlHelper = new SqlHelper();

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetUserList");

                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        userList.Add(new AppUser
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            Email = dr["Email"].ToString()

                        });
                    }
                }
            }
            catch (Exception ex)
            {

                General.LogException(ex);

            }
            return userList;
        }

        public bool SaveUser()
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Email", SqlDbType.VarChar, Email);
                sqlHelper.AddSetParameterToSQLCommand("@Password", SqlDbType.VarChar, Password);
                sqlHelper.AddSetParameterToSQLCommand("@Created_by", SqlDbType.Int, Created_by);


                sqlHelper.GetExecuteNonQueryByCommand("SP_SaveUser");

                return true;
            }
            catch (Exception ex)
            {

                General.LogException(ex);

            }

            return false;
        }

        public bool DeleteUser(int id)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);


                sqlHelper.GetExecuteNonQueryByCommand("SP_DeleteUser");

                return true;
            }
            catch (Exception ex)
            {

                General.LogException(ex);

            }

            return false;
        }
    }
}