﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace SocialWall.Admin.Models
{
    public class Walls
    {
        public List<Wall> wallList { get; set; }
    }

    public class Wall
    {
        public int Id { get; set; }
        public string Wall_name { get; set; }
        public string Wall_title { get; set; }
        public string Wall_details { get; set; }
        public bool IsEnabled { get; set; }
        public int Wall_refresh_seconds { get; set; }
        public string Created_by { get; set; }
        


        public List<Wall> GetWallList()
        {
            List<Wall> wallList = new List<Wall>();

            try
            {
                
                SqlHelper sqlHelper = new SqlHelper();

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetWallList");

                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        wallList.Add(new Wall
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            Wall_name = dr["Wall_name"].ToString(),
                            Wall_title = dr["Wall_title"].ToString(),
                            Wall_details = dr["Wall_details"].ToString(),
                            Wall_refresh_seconds = Convert.ToInt32(dr["Wall_refresh_seconds"].ToString()),
                            IsEnabled = bool.Parse(dr["IsEnabled"].ToString())
                        });
                    }
                }
                
            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return wallList;

        }

        //// GET api/<controller>/5
        public Wall Get(int id)
        {
            Wall wall = new Wall();

            try
            {
               
                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetWall");

                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        wall = new Wall
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            Wall_name = dr["Wall_name"].ToString(),
                            Wall_title = dr["Wall_title"].ToString(),
                            Wall_details = dr["Wall_details"].ToString(),
                            Wall_refresh_seconds = Convert.ToInt32(dr["Wall_refresh_seconds"].ToString()),
                            IsEnabled = bool.Parse(dr["IsEnabled"].ToString())
                        };
                    }
                }
                
            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return wall;
        }

        // POST api/<controller>
        public bool SaveWall()
        {
            ReturnData returnData = new ReturnData();
           
            try
            {
                
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Wall_name", SqlDbType.VarChar, Wall_name);
                sqlHelper.AddSetParameterToSQLCommand("@Wall_title", SqlDbType.VarChar, Wall_title);
                sqlHelper.AddSetParameterToSQLCommand("@Wall_details", SqlDbType.VarChar, Wall_details);
                sqlHelper.AddSetParameterToSQLCommand("@Wall_refresh_seconds", SqlDbType.Int, Wall_refresh_seconds);
                sqlHelper.AddSetParameterToSQLCommand("@Created_by", SqlDbType.Int, Created_by);

                sqlHelper.GetExecuteNonQueryByCommand("SP_SaveWall");

                return true;
                
            }
            catch (Exception ex)
            {
                returnData.operationStatus = "EXCEPTION";
                General.LogException(ex);

            }

            

            return false;
        }

        // PUT api/<controller>/5

        public bool Update(int id , string nsvar)
        {
            
            SqlHelper sqlHelper = new SqlHelper();
          
            if (nsvar == "STATUSUPDATE")
            {
                try
                {
                    sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);
                    sqlHelper.AddSetParameterToSQLCommand("@IsEnabled", SqlDbType.Bit, IsEnabled);

                    sqlHelper.GetExecuteNonQueryByCommand("SP_UpdateWall_SetStatus");

                    return true;
                    
                }
                catch (Exception ex)
                {
                   
                    General.LogException(ex);

                }

            }

            else
            {
                try
                {
                   


                    sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);
                    sqlHelper.AddSetParameterToSQLCommand("@Wall_name", SqlDbType.VarChar, Wall_name);
                    sqlHelper.AddSetParameterToSQLCommand("@Wall_title", SqlDbType.VarChar, Wall_title);
                    sqlHelper.AddSetParameterToSQLCommand("@Wall_details", SqlDbType.VarChar, Wall_details);
                    sqlHelper.AddSetParameterToSQLCommand("@Wall_refresh_seconds", SqlDbType.Int, Wall_refresh_seconds);

                    sqlHelper.GetExecuteNonQueryByCommand("SP_UpdateWall");

                    return true;


                }
                catch (Exception ex)
                {
                   
                    General.LogException(ex);

                }
            }

            return false;
        }


        // DELETE api/<controller>/5
        public bool Delete(int id)
        {
          
            try
            {

                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);


                sqlHelper.GetExecuteNonQueryByCommand("SP_DeleteWall");

                return true;
            }
            catch (Exception ex)
            {
               
                General.LogException(ex);

            }

            return false;
        }

    }
    public class WallViewModel
    {
        [Required(ErrorMessage = "Wall name required")]
        [DataType(DataType.Text, ErrorMessage = "Wall name is not valid")]
        public string Wall_name { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Wall name is not valid")]
        public string Wall_title { get; set; }
        public string Wall_details { get; set; }

        [Required(ErrorMessage = "Refresh interval required")]
        [Range(1, int.MaxValue, ErrorMessage = "Refresh interval is not valid")]
        public int Wall_refresh_seconds { get; set; }

        public bool IsEnabled { get; set; }

        public string nsvar { get; set; }
    }
}