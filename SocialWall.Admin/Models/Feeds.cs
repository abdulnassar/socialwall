﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SocialWall.Admin.Models
{
    public class Feeds
    {
        public List<Feed> feedList { get; set; }
    }

    public class Feed
    {
        public int Id { get; set; }
        public string Social_id { get; set; }
        public string Social_image_thumbnail { get; set; }
        public string Social_image_low_resolution { get; set; }

        public string Social_image_standard_resolution { get; set; }
        public string Social_caption_text { get; set; }
        public string Social_user_id { get; set; }
        public string Social_user_full_name { get; set; }
        public string Social_user_profile_picture { get; set; }
        public string Social_user_username { get; set; }
        public string Feed_source_type { get; set; }
        public string Feed_source_social_id { get; set; }
        public string Feed_socialmedia_name { get; set; }
        public bool IsEnabled { get; set; }
        public string Social_likes_count { get; set; }
        public string Social_tags_count { get; set; }
        public string Social_comments_count { get; set; }


        public void FilterFeed()
        {
            SqlHelper sqlHelper = new SqlHelper();

            sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, Id);
            sqlHelper.AddSetParameterToSQLCommand("@Feed_socialmedia_name", SqlDbType.VarChar, Feed_socialmedia_name);
            sqlHelper.AddSetParameterToSQLCommand("@Feed_source_social_id", SqlDbType.VarChar, Feed_source_social_id);
            sqlHelper.AddSetParameterToSQLCommand("@IsEnabled", SqlDbType.Bit, IsEnabled);
            sqlHelper.AddSetParameterToSQLCommand("@Created_by", SqlDbType.Int, HttpContext.Current.Session["UserId"].ToString());
            sqlHelper.GetExecuteNonQueryByCommand("SP_FilterFeed");
        }

    }

    public class FeedViewModel
    {
        
        public string Feed_source_social_id { get; set; }

     
        public string Feed_socialmedia_name { get; set; }
       

        public bool IsEnabled { get; set; }

        
    }
}