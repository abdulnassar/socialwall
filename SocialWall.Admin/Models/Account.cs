﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace SocialWall.Admin.Models
{
    public class Account
    {
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        //[Required]
        //[Display(Name = "Username")]        
        //public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[Display(Name = "Remember me?")]
        //public bool RememberMe { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

      
    }
  
    public class AccountModel
    {
        
        public string Email { get; set; }
        public string Password_token { get; set; }
        public string Password { get; set; }

        public bool SaveForgotPassword()
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Email", SqlDbType.VarChar, Email);
                sqlHelper.AddSetParameterToSQLCommand("@Password_token", SqlDbType.VarChar, Password_token);

                sqlHelper.GetExecuteNonQueryByCommand("SP_SavePasswordToken");

                return true;
            }
            catch (Exception ex)
            {
                General.LogException(ex);
             
            }
            return false;
        }

        public bool ValidatePasswordToken()
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Email", SqlDbType.VarChar, Email);
                sqlHelper.AddSetParameterToSQLCommand("@Password_token", SqlDbType.VarChar, Password_token);
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_ValidatePasswordToken");

                if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                {
                   
                    return true;
                }
               
            }
            catch (Exception ex)
            {
                General.LogException(ex);

            }
            return false;
        }

        public bool ResetPAssword()
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Email", SqlDbType.VarChar, Email);
                sqlHelper.AddSetParameterToSQLCommand("@Password_token", SqlDbType.VarChar, Password_token);
                sqlHelper.AddSetParameterToSQLCommand("@Password", SqlDbType.VarChar, Password);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_ResetPassword");

                if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                {
                    return true;


                }

            }
            catch (Exception ex)
            {
                General.LogException(ex);

            }
            return false;
        }

        public bool Login()
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Email", SqlDbType.VarChar, Email);
                sqlHelper.AddSetParameterToSQLCommand("@Password", SqlDbType.VarChar, Password);

                DataSet ds = sqlHelper.GetDatasetByCommand("[SP_GetUser]");

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        HttpContext.Current.Session["UserId"] = ds.Tables[0].Rows[0]["Id"].ToString();


                        string UserRoles = "Admin,Moderator";

                        HttpContext.Current.Session["privilegeLevels"] = UserRoles;

                        return true;
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                General.LogException(ex);

            }
            return false;
        }
    }


    public class ResetPasswordViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Password is too short")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password")]
        public string Password1 { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Token")]
        public string Token { get; set; }

    }
}