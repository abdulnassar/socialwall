﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Data;

namespace SocialWall.SocialConnect.Instagram
{
    public class Insta
    {
        public async System.Threading.Tasks.Task<string> LoadInstagramFeeds(string Source_social_id, string Source_type)
        {
           
            HttpClient client;
            SqlHelper sqlHelper;
            string apiUrl = "";
            string access_token = General.GetSettingsValue("Instagram_access_token"); //get instagram access token 
                                   

            if (Source_type == "User")// if feed request is for a user account
            {
                apiUrl = General.GetSettingsValue("Instagram_users_endpoint_base") + Source_social_id + "/media/recent/?access_token="+ access_token;

            }
            else if (Source_type == "Tag")// if feed request is for a tag
            {
                apiUrl = General.GetSettingsValue("Instagram_users_endpoint_base") + Source_social_id + "/media/recent?access_token="+ access_token;

            }

            goto ApiCall;

            ApiCall:
            try
            {
                client = new HttpClient();

                var response = await
                    client.GetAsync(apiUrl);

                var responseString = await response.Content.ReadAsStringAsync();

                InstagramFeed instagramFeed = Newtonsoft.Json.JsonConvert.DeserializeObject<InstagramFeed>(responseString);

                if (instagramFeed.meta.code == 200) //save feeds in db
                {
                    try
                    {
                        foreach (Datum datum in instagramFeed.data)
                        {
                            sqlHelper = new SqlHelper();

                            sqlHelper.AddSetParameterToSQLCommand("@Social_id", SqlDbType.VarChar, datum.id);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_image_thumbnail", SqlDbType.VarChar, datum.images.thumbnail.url);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_image_low_resolution", SqlDbType.VarChar, datum.images.low_resolution.url);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_image_standard_resolution", SqlDbType.VarChar, datum.images.standard_resolution.url);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_caption_text", SqlDbType.NVarChar, datum.caption.text);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_user_id", SqlDbType.VarChar, datum.user.id);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_user_full_name", SqlDbType.VarChar, datum.user.full_name);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_user_profile_picture", SqlDbType.VarChar, datum.user.profile_picture);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_user_username", SqlDbType.VarChar, datum.user.username);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_from_id", SqlDbType.VarChar, datum.caption.from.id);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_from_full_name", SqlDbType.VarChar, datum.caption.from.full_name);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_from_profile_picture", SqlDbType.VarChar, datum.caption.from.profile_picture);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_from_username", SqlDbType.VarChar, datum.caption.from.username);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_likes_count", SqlDbType.Int, datum.likes.count);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_tags_count", SqlDbType.Int, datum.tags.Length);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_comments_count", SqlDbType.Int, datum.comments.count);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_link", SqlDbType.VarChar, datum.link);
                            sqlHelper.AddSetParameterToSQLCommand("@Feed_socialmedia_name", SqlDbType.VarChar, "Instagram");
                            sqlHelper.AddSetParameterToSQLCommand("@Feed_source_type", SqlDbType.VarChar, Source_type);
                            sqlHelper.AddSetParameterToSQLCommand("@Feed_source_social_id", SqlDbType.VarChar, Source_social_id);
                            sqlHelper.AddSetParameterToSQLCommand("@Social_created_time", SqlDbType.VarChar, datum.created_time);

                            

                            sqlHelper.GetExecuteNonQueryByCommand("SP_SaveFeed");
                        }

                        if (instagramFeed.pagination != null)
                        {
                            if (instagramFeed.pagination.next_url != null)
                            {
                                apiUrl = instagramFeed.pagination.next_url;
                                goto ApiCall;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        General.LogException(ex);
                        return "exception";
                    }                   

                    return "success";
                }
                else
                {
                    if(instagramFeed.meta.error_type != null)
                    {
                        if(instagramFeed.meta.error_type== "OAuthAccessTokenException")
                        {
                            //save auth failure in db to notify the admin user to re-authenticate
                            General.SetSettingsValue("Instagram_access_token_status", "EXPIRED");
                        }
                    }
                  
                    return "failure";
                }               
             

            }
            catch (Exception ex)
            {
                General.LogException(ex);
                return "exception";
            }

           
        }
    }
}