﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialWall.SocialConnect.Instagram
{  


    public class InstagramFeed
    {
        public Pagination pagination { get; set; }
        public Datum[] data { get; set; }
        public Meta meta { get; set; }
    }

    public class Pagination
    {
        public string next_url { get; set; }
        public string next_max_id { get; set; }
    }

    public class Meta
    {
        public int code { get; set; }
        public string error_type { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public User user { get; set; }
        public Images images { get; set; }
        public string created_time { get; set; }
        public Caption caption { get; set; }
        public bool user_has_liked { get; set; }
        public Likes likes { get; set; }
        public object[] tags { get; set; }
        public string filter { get; set; }
        public Comments comments { get; set; }
        public string type { get; set; }
        public string link { get; set; }
        public object location { get; set; }
        public object attribution { get; set; }
        public object[] users_in_photo { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public string full_name { get; set; }
        public string profile_picture { get; set; }
        public string username { get; set; }
    }

    public class Images
    {
        public Thumbnail thumbnail { get; set; }
        public Low_Resolution low_resolution { get; set; }
        public Standard_Resolution standard_resolution { get; set; }
    }

    public class Thumbnail
    {
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }
    }

    public class Low_Resolution
    {
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }
    }

    public class Standard_Resolution
    {
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }
    }

    public class Caption
    {
        public string id { get; set; }
        public string text { get; set; }
        public string created_time { get; set; }
        public From from { get; set; }
    }

    public class From
    {
        public string id { get; set; }
        public string full_name { get; set; }
        public string profile_picture { get; set; }
        public string username { get; set; }
    }

    public class Likes
    {
        public int count { get; set; }
    }

    public class Comments
    {
        public int count { get; set; }
    }

}