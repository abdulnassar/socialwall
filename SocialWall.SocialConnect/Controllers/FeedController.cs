﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace SocialWall.SocialConnect.Controllers
{
    public class FeedController : ApiController
    {
        // GET api/<controller>
        
        public async System.Threading.Tasks.Task<string> Get()
        {
            try
            {
               
                SqlHelper sqlHelper;
                Instagram.Insta instagram;
                //get unique feed sources from db
                sqlHelper = new SqlHelper();
                DataSet ds = sqlHelper.GetDatasetByCommand("[SP_GetSources]");

                //loop through and request to instagram for feeds
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    instagram = new Instagram.Insta();
                    if (dr["Source_socialmedia_name"].ToString() == "Instagram")
                    {
                        var responseString = await instagram.LoadInstagramFeeds(dr["Source_social_id"].ToString(), dr["Source_type"].ToString());
                    }
                    
                }
              

            }
            catch(Exception ex)
            {
                General.LogException(ex);
            }
            return "success";
            
        }

        

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}