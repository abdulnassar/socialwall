﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SocialWall.Admin.Models;
using System.Data;
using SocialWall.Client.Models;

namespace SocialWall.Admin.Controllers
{
    
    public class WallApiController : ApiController
    {
        // GET api/<controller>
        public List<Wall> Get()
        {
            List<Wall> wallList = new List<Wall>();

            try
            {
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    SqlHelper sqlHelper = new SqlHelper();

                    DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetWallList");

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            wallList.Add(new Wall
                            {
                                Id = Convert.ToInt32(dr["Id"].ToString()),
                                Wall_name = dr["Wall_name"].ToString(),
                                Wall_title = dr["Wall_title"].ToString(),
                                Wall_details = dr["Wall_details"].ToString(),
                                Wall_refresh_seconds = Convert.ToInt32(dr["Wall_refresh_seconds"].ToString()),
                                IsEnabled = bool.Parse(dr["IsEnabled"].ToString())
                            });
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                General.LogException(ex);
            }
            return wallList;

        }

        //// GET api/<controller>/5
        public ReturnData Get(int id)
        {
            ReturnData rData = new ReturnData();
            
            Wall wall = new Wall();

            try
            {

                if (wall.Get(id))
                {
                    rData.operationStatus = "SUCCESS";
                    rData.data = wall;
                }

             


            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return rData;
        }

        
    }
}