﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Net;
using System.Configuration;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

public class General
    {
       

        public static string GetIPAddress()
        {
           
            string sIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (sIPAddress != null)
            {
                string[] ipArray = sIPAddress.Split(',');
                return ipArray[0];
            }

            else
            {
                return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            
        }


        public static string GetSettingsValue(string key)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@SettingsName", SqlDbType.VarChar, key);
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetSettingsValue");


                if (ds.Tables.Count > 0)
                {

                    return ds.Tables[0].Rows[0]["SettingsValue"].ToString();
                }
                return null;
            }
            catch (Exception ex)
            {
               General.LogException(ex);
               return null;
            }
        }

        public static string SetSettingsValue(string key, string value)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@SettingsName", SqlDbType.VarChar, key);
                sqlHelper.AddSetParameterToSQLCommand("@SettingsValue", SqlDbType.VarChar, value);
                sqlHelper.GetExecuteNonQueryByCommand("SP_SetSettingsValue");



                return "success";
            }
            catch (Exception ex)
            {
              General.LogException(ex);

              return null;
            }
        }

        public static void LogException(Exception ex)
        {
            try
            {
                string FileName = HttpContext.Current.Server.MapPath(@"~\App_Data\Logs\error_" + DateTime.Today.ToString("yyyMMdd") + ".txt");
                if (!CheckIfLogFileExists(FileName))//log file not exists
                {
                    CreateLogFile(FileName); //create log file
                }
                if (CheckIfLogFileExists(FileName))
                {
                    SaveException(FileName, ex);
                }

            }
            catch
            {
            }
        }
        private static bool SaveException(string fileName, string message)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(fileName))
                {
                    sw.WriteLine(DateTime.Now.ToString());

                    StringBuilder s = new StringBuilder();

                    sw.WriteLine("Message: " + message);

                    sw.WriteLine();
                    sw.WriteLine();

                }
                return true;
            }
            catch
            {
            }
            return false;
        }
        private static bool SaveException(string fileName, Exception ex)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(fileName))
                {
                    sw.WriteLine(DateTime.Now.ToString());
                    sw.WriteLine("ex: " + ex.ToString());
                    sw.WriteLine("ex.Message: " + ex.Message);
                    sw.WriteLine("ex.StackTrace: " + ex.StackTrace);
                    sw.WriteLine("ex.Source: " + ex.Source);
                    StringBuilder s = new StringBuilder();
                    while (ex != null)
                    {
                        s.AppendLine("Exception type: " + ex.GetType().FullName);
                        s.AppendLine("Message       : " + ex.Message);
                        s.AppendLine("Stacktrace:");
                        s.AppendLine(ex.StackTrace);
                        s.AppendLine();
                        ex = ex.InnerException;
                    }
                    sw.WriteLine("innerexceptions: " + s);

                    sw.WriteLine();
                    sw.WriteLine();

                }
                return true;
            }
            catch
            {
            }
            return false;
        }
        private static bool CreateLogFile(string fileName)
        {
            try
            {
                File.CreateText(fileName).Close();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        private static bool CheckIfLogFileExists(string fileName)
        {
            try
            {
                return File.Exists(fileName);

            }
            catch
            {
            }
            return false;
        }

        private static int DEFAULT_MIN_PASSWORD_LENGTH = 4;
        private static int DEFAULT_MAX_PASSWORD_LENGTH = 10;

        // Define supported password characters divided into groups.
        // You can add (or remove) characters to (from) these groups.
        private static string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
        private static string PASSWORD_CHARS_UCASE = "ABCDEFGHJKLMNPQRSTWXYZ";
        private static string PASSWORD_CHARS_NUMERIC = "23456789";
        private static string PASSWORD_CHARS_SPECIAL = "$?_&!%";

        public static string GeneratePassword()
        {
            return Generate(DEFAULT_MIN_PASSWORD_LENGTH,
                            DEFAULT_MAX_PASSWORD_LENGTH);
        }

        public static string GenerateOTP(int length)
        {
          PASSWORD_CHARS_LCASE = "0123456789";
          PASSWORD_CHARS_UCASE = "0123456789";
          PASSWORD_CHARS_NUMERIC = "0123456789";
          PASSWORD_CHARS_SPECIAL = "0123456789";
            return Generate(length,
                            length);
        }
        public static string Generate(int length)
        {
            return Generate(length, length);
        }
        public static string Generate(int minLength,
                                    int maxLength)
        {
            // Make sure that input parameters are valid.
            if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                return null;

            // Create a local array containing supported password characters
            // grouped by types. You can remove character groups from this
            // array, but doing so will weaken the password strength.
            char[][] charGroups = new char[][] 
        {
            PASSWORD_CHARS_LCASE.ToCharArray(),
            PASSWORD_CHARS_UCASE.ToCharArray(),
            PASSWORD_CHARS_NUMERIC.ToCharArray(),
            PASSWORD_CHARS_SPECIAL.ToCharArray()
        };

            // Use this array to track the number of unused characters in each
            // character group.
            int[] charsLeftInGroup = new int[charGroups.Length];

            // Initially, all characters in each group are not used.
            for (int i = 0; i < charsLeftInGroup.Length; i++)
                charsLeftInGroup[i] = charGroups[i].Length;

            // Use this array to track (iterate through) unused character groups.
            int[] leftGroupsOrder = new int[charGroups.Length];

            // Initially, all character groups are not used.
            for (int i = 0; i < leftGroupsOrder.Length; i++)
                leftGroupsOrder[i] = i;

            // Because we cannot use the default randomizer, which is based on the
            // current time (it will produce the same "random" number within a
            // second), we will use a random number generator to seed the
            // randomizer.

            // Use a 4-byte array to fill it with random bytes and convert it then
            // to an integer value.
            byte[] randomBytes = new byte[4];

            // Generate 4 random bytes.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = BitConverter.ToInt32(randomBytes, 0);

            // Now, this is real randomization.
            Random random = new Random(seed);

            // This array will hold password characters.
            char[] password = null;

            // Allocate appropriate memory for the password.
            if (minLength < maxLength)
                password = new char[random.Next(minLength, maxLength + 1)];
            else
                password = new char[minLength];

            // Index of the next character to be added to password.
            int nextCharIdx;

            // Index of the next character group to be processed.
            int nextGroupIdx;

            // Index which will be used to track not processed character groups.
            int nextLeftGroupsOrderIdx;

            // Index of the last non-processed character in a group.
            int lastCharIdx;

            // Index of the last non-processed group.
            int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

            // Generate password characters one at a time.
            for (int i = 0; i < password.Length; i++)
            {
                // If only one character group remained unprocessed, process it;
                // otherwise, pick a random character group from the unprocessed
                // group list. To allow a special character to appear in the
                // first position, increment the second parameter of the Next
                // function call by one, i.e. lastLeftGroupsOrderIdx + 1.
                if (lastLeftGroupsOrderIdx == 0)
                    nextLeftGroupsOrderIdx = 0;
                else
                    nextLeftGroupsOrderIdx = random.Next(0,
                                                         lastLeftGroupsOrderIdx);

                // Get the actual index of the character group, from which we will
                // pick the next character.
                nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];

                // Get the index of the last unprocessed characters in this group.
                lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

                // If only one unprocessed character is left, pick it; otherwise,
                // get a random character from the unused character list.
                if (lastCharIdx == 0)
                    nextCharIdx = 0;
                else
                    nextCharIdx = random.Next(0, lastCharIdx + 1);

                // Add this character to the password.
                password[i] = charGroups[nextGroupIdx][nextCharIdx];

                // If we processed the last character in this group, start over.
                if (lastCharIdx == 0)
                    charsLeftInGroup[nextGroupIdx] =
                                              charGroups[nextGroupIdx].Length;
                // There are more unprocessed characters left.
                else
                {
                    // Swap processed character with the last unprocessed character
                    // so that we don't pick it until we process all characters in
                    // this group.
                    if (lastCharIdx != nextCharIdx)
                    {
                        char temp = charGroups[nextGroupIdx][lastCharIdx];
                        charGroups[nextGroupIdx][lastCharIdx] =
                                    charGroups[nextGroupIdx][nextCharIdx];
                        charGroups[nextGroupIdx][nextCharIdx] = temp;
                    }
                    // Decrement the number of unprocessed characters in
                    // this group.
                    charsLeftInGroup[nextGroupIdx]--;
                }

                // If we processed the last group, start all over.
                if (lastLeftGroupsOrderIdx == 0)
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                // There are more unprocessed groups left.
                else
                {
                    // Swap processed group with the last unprocessed group
                    // so that we don't pick it until we process all groups.
                    if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                    {
                        int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                        leftGroupsOrder[lastLeftGroupsOrderIdx] =
                                    leftGroupsOrder[nextLeftGroupsOrderIdx];
                        leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                    }
                    // Decrement the number of unprocessed groups.
                    lastLeftGroupsOrderIdx--;
                }
            }

            // Convert password characters into a string and return the result.
            return new string(password);
        }

        public static bool SendEmail(string subject,string body,string from,string fromDisplayName, string to)
        {
       

            try
            {
            #region PrepareEmail

          
                MailMessage mailMessage = new MailMessage();
                mailMessage.To.Add(to);
                mailMessage.From = new MailAddress(from, fromDisplayName);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                SmtpClient smtpClient = new SmtpClient(General.GetSettingsValue("SmtpIp"), int.Parse(General.GetSettingsValue("SmtpPort")));

                #endregion

                smtpClient.Send(mailMessage);

                return true;

            }
            catch (Exception ex)
            {
                General.LogException(ex);
                return false;
            }
        }

}


public class AuthorizeUserAttribute : AuthorizeAttribute
{
   
    public string AccessLevel { get; set; }

    protected override bool AuthorizeCore(HttpContextBase httpContext)

    {
        var isAuthorized = false;

        //HttpContext.Current.Session["privilegeLevels"] = "Admin,Operations,SuperAdmin";
        //HttpContext.Current.Session["nameOfUser"] = "Abdul Nassar Thalayancheril";
        //HttpContext.Current.Session["UserID"] = "101";

        if (HttpContext.Current.Session["privilegeLevels"] != null)
        {
            string[] privilegeLevels = HttpContext.Current.Session["privilegeLevels"].ToString().Split(',');
            string[] AccessLevels = this.AccessLevel.Split(',');

            foreach (string accessLevel in AccessLevels)
            {
                if (privilegeLevels.Contains(accessLevel))
                {
                    isAuthorized = true;
                    break;
                }
            }


        }

        return isAuthorized;
    }
}
