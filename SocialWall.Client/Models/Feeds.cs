﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialWall.Client.Models
{
 
    public class Feed
    {
        //public int Id { get; set; }
        public string Social_id { get; set; }
        public string Social_image_thumbnail { get; set; }
        public string Social_image_low_resolution { get; set; }

        public string Social_image_standard_resolution { get; set; }
        public string Social_caption_text { get; set; }
        public string Social_user_id { get; set; }
        public string Social_user_full_name { get; set; }
        public string Social_user_profile_picture { get; set; }
        public string Social_user_username { get; set; }
        public string Feed_source_type { get; set; }
        public string Feed_source_social_id { get; set; }
        public string Feed_socialmedia_name { get; set; }
        //public bool IsEnabled { get; set; }
      

    }

   
}