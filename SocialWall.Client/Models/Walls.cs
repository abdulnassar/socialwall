﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace SocialWall.Client.Models
{
    public class Walls
    {
        public List<Wall> wallList { get; set; }
    }

    public class Wall
    {
        public int Id { get; set; }
        public string Wall_name { get; set; }
        public string Wall_title { get; set; }
        public string Wall_details { get; set; }
        public bool IsEnabled { get; set; }
        public int Wall_refresh_seconds { get; set; }

        public List<Feed> feedList { get; set; }

        public bool Get(int id)
        {
           

            

            try
            {

                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, id);
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetWall_Client");

                if (ds.Tables.Count > 1)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        Id = Convert.ToInt32(dr["Id"].ToString());
                        Wall_name = dr["Wall_name"].ToString();
                        Wall_title = dr["Wall_title"].ToString();
                        Wall_details = dr["Wall_details"].ToString();
                        Wall_refresh_seconds = Convert.ToInt32(dr["Wall_refresh_seconds"].ToString());
                        IsEnabled = bool.Parse(dr["IsEnabled"].ToString());

                    }

                    feedList = new List<Feed>();

                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        feedList.Add(new Feed
                        {
                            Social_id = dr["Social_id"].ToString(),
                            Social_image_thumbnail = dr["Social_image_thumbnail"].ToString(),
                            Social_image_low_resolution = dr["Social_image_low_resolution"].ToString(),
                            Social_image_standard_resolution = dr["Social_image_standard_resolution"].ToString(),
                            Social_caption_text = dr["Social_caption_text"].ToString(),
                            Social_user_id = dr["Social_user_id"].ToString(),
                            Social_user_full_name = dr["Social_user_full_name"].ToString(),
                            Social_user_profile_picture = dr["Social_user_profile_picture"].ToString(),
                            Social_user_username = dr["Social_user_username"].ToString(),
                            Feed_socialmedia_name = dr["Feed_socialmedia_name"].ToString()

                        });
                    }

                  
                }

                return true;


            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return false;
        }
    }
   
}